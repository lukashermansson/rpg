package main.java.rpg.Entites;

import java.lang.reflect.Method;

import org.bukkit.Location;
import org.bukkit.entity.EntityType;

import net.minecraft.server.v1_11_R1.EntityCow;
import net.minecraft.server.v1_11_R1.EntityInsentient;
import net.minecraft.server.v1_11_R1.EntityTypes;
import net.minecraft.server.v1_11_R1.World;

public class RPGMob{
	public enum CustomEntityType {
		 
		COW("Cow", 92, EntityType.COW, EntityCow.class, customCow.class);
		 
	    private String name;
	    private int id;
	    private EntityType entityType;
	    private Class<? extends EntityInsentient> nmsClass;
	    private Class<? extends EntityInsentient> customClass;
	 
	    private CustomEntityType(String name, int id, EntityType entityType, Class<? extends EntityInsentient> nmsClass, Class<? extends EntityInsentient> customClass){
	        this.name = name;
	        this.id = id;
	        this.entityType = entityType;
	        this.nmsClass = nmsClass;
	        this.customClass = customClass;
	    }

		public static void spawnCustomEntity(CustomEntityType entity, String name, Location l, World world) {
	        customCow zomb = new customCow(world);
	        zomb.setCustomName("Test");
	        zomb.setCustomNameVisible(true);
	        zomb.setPositionRotation(l.getX(), l.getY(), l.getZ(), l.getYaw(), l.getPitch());
	    }
	 
	    public String getName(){
	        return this.name;
	    }
	 
	    public int getID(){
	        return this.id;
	    }
	 
	    public EntityType getEntityType(){
	        return this.entityType;
	    }
	 
	    public Class<? extends EntityInsentient> getNMSClass(){
	        return this.nmsClass;
	    }
	 
	    public Class<? extends EntityInsentient> getCustomClass(){
	        return this.customClass;
	    }
	 
	    public static void registerEntities(){
	        for (CustomEntityType entity : values()){
	            try{
	                Method a = EntityTypes.class.getDeclaredMethod("a", new Class<?>[]{Class.class, String.class, int.class});
	                a.setAccessible(true);
	                a.invoke(null, entity.getCustomClass(), entity.getName(), entity.getID());
	            }catch (Exception e){
	                e.printStackTrace();
	            }
	        }
	    }
	}
}