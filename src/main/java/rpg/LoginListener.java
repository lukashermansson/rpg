package rpg;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.concurrent.TimeUnit;

import org.bukkit.ChatColor;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.player.PlayerJoinEvent;
import org.bukkit.event.player.PlayerLoginEvent;
import org.bukkit.event.player.PlayerLoginEvent.Result;
import org.bukkit.event.player.PlayerQuitEvent;

public class LoginListener implements Listener {
	RPGPlugin plugin = RPGPlugin.getPlugin();

	@EventHandler
	public void onPlayerLogin(PlayerLoginEvent event) {

		if (IsBanned(event.getPlayer().getUniqueId().toString())) {
			// we know it is a temp ban
			if (GetTime(event.getPlayer().getUniqueId().toString()) != null) {
				// clac diff
				// This have to look as in the database
				DateFormat format = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");

				// this is how it will print
				DateFormat printFormat = new SimpleDateFormat("yyyy.MM.dd HH:mm");
				Date d1 = null;
				try {
					d1 = format.parse(GetTime(event.getPlayer().getUniqueId().toString()));
				} catch (ParseException e) {
					e.printStackTrace();
				}

				event.disallow(Result.KICK_BANNED,
						ChatColor.translateAlternateColorCodes('&',
								"&4You are temporarely banned from " + RPGPlugin.pluginString
										+ " &4you are unbanned on " + printFormat.format(d1) + " with reason: "
										+ GetReason(event.getPlayer().getUniqueId().toString())));
			} else {
				// this is a permanent ban
				event.disallow(Result.KICK_BANNED,
						ChatColor.translateAlternateColorCodes('&',
								"&4You are permanently banned from " + RPGPlugin.pluginString + " &4with reason: "
										+ GetReason(event.getPlayer().getUniqueId().toString())));
			}
		}
	}

	@EventHandler
	public void onPlayerJoin(PlayerJoinEvent event) {
		RPGPlayer rpg = new RPGPlayer(event.getPlayer());
		rpg.registerPlayer();

		// Guild stuff
		// load guild from db
		try {
			Statement statement = plugin.GetConnection().createStatement();
			ResultSet res = statement.executeQuery(
					"SELECT * FROM Guildmember WHERE player_UUID = '" + rpg.getBukkitPlayer().getUniqueId() + "';");
			if (res.next()) {
				// player is in a guild
				int GuildId = res.getInt("Guild");
				if (Guild.getGuildFromId(GuildId) != null) {
					// Guild is loaded
					Logger.Logg("Loading guild from memmory", LogLevel.debug);
					Guild guild = Guild.getGuildFromId(GuildId);
					if (guild != null) {
						guild.AddLoggedInPlayer(rpg.getBukkitPlayer().getUniqueId() + "");
					}
				} else {
					// guild is not loaded, create one
					Logger.Logg("Creating guild to memory", LogLevel.debug);
					Guild guild = new Guild(GuildId);
					guild.AddLoggedInPlayer(rpg.getBukkitPlayer().getUniqueId() + "");
				}
			}
			statement.close();
			res.close();
		} catch (SQLException e) {
			Logger.Logg("SQL for loading guild failed");
			e.printStackTrace();
		}
	}

	private String GetReason(String UUID) {
		// If a ban for id is not pressent this will NPE
		Statement statement;
		DateFormat dateFormat = new SimpleDateFormat("yyyyMMddHHmmss");
		Date date = new Date();
		try {
			statement = plugin.GetConnection().createStatement();

			// gets all results with a date witch is further from current than
			// current
			ResultSet res = statement.executeQuery("SELECT * FROM Bans WHERE Victim = '" + UUID + "' AND (Until > '"
					+ dateFormat.format(date) + "' OR Until IS NULL);");
			// assuming we know a ban is valid we don't need a check
			res.next();
			return res.getString("Reason");
		} catch (SQLException e) {
			e.printStackTrace();
			return null;
		}
	}

	public static long getDateDiff(Date date1, Date date2, TimeUnit timeUnit) {
		long diffInMillies = date2.getTime() - date1.getTime();
		return timeUnit.convert(diffInMillies, TimeUnit.MILLISECONDS);
	}

	private String GetTime(String UUID) {
		Statement statement;
		try {
			DateFormat dateFormat = new SimpleDateFormat("yyyyMMddHHmmss");
			Date date = new Date();
			statement = plugin.GetConnection().createStatement();
			// gets all results with a date witch is further from current than
			// current
			ResultSet res = statement.executeQuery("SELECT * FROM Bans WHERE Victim = '" + UUID + "' AND (Until > '"
					+ dateFormat.format(date) + "');");
			statement.close();
			if (res.next()) {
				String Util = res.getString("Until");
				res.close();
				return Util;
			} else {
				res.close();
				return null;
			}

		} catch (SQLException e) {
			// if this happens we return with yes for everybody
			e.printStackTrace();
			Logger.Logg("Error in Time lokup for temp bans for UUID: " + UUID);
			return null;
		}

	}

	@EventHandler
	private void onPlayerLogout(PlayerQuitEvent event) {
		RPGPlayer rpg = RPGPlayer.getRpgPlayerFromBukkitPlayer(event.getPlayer());
		if (rpg != null) {
			rpg.UnregisterPlayer();
			try {
				Statement statement;
				statement = plugin.GetConnection().createStatement();
				ResultSet res = null;
				res = statement.executeQuery(
                        "SELECT Guild FROM Guildmember WHERE player_UUID = '" + rpg.getBukkitPlayer().getUniqueId() + "';");
				if (res.next()) {
					int GuildId = res.getInt("Guild");
					Guild guild = Guild.getGuildFromId(GuildId);
					if (guild != null) {
						GuildMember mem = guild.GetGuildMemberFromUUID(rpg.getBukkitPlayer().getUniqueId() + "");
						if (mem != null) {
							guild.AttemptRemoveOfJustOnlinePlayer(mem);
						}
						guild.TryUnload();
					}
				}
			} catch (SQLException e) {
				Logger.Logg("SQl exception In removal of online state in guild");
				e.printStackTrace();
			}
		}else{

			Logger.Logg("Cant find player that's logging out");
		}
	}

	// this function will return if the player is banned nothing more nothing
	// less
	private boolean IsBanned(String UUID) {
		Statement statement;
		try {
			DateFormat dateFormat = new SimpleDateFormat("yyyyMMddHHmmss");
			Date date = new Date();
			statement = plugin.GetConnection().createStatement();
			// gets all results with a date witch is further from current than
			// current
			ResultSet res = statement.executeQuery("SELECT * FROM Bans WHERE Victim = '" + UUID + "' AND (Until > '"
					+ dateFormat.format(date) + "' OR Until IS NULL);");

			// player has a ban on record
			return res.next();

		} catch (SQLException e) {
			// if this happens we return with yes for everybody
			e.printStackTrace();
			Logger.Logg("SQl exception in BanLookup for uuid: " + UUID);
			return true;
		}

	}
}
