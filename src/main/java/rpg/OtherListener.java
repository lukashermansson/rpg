package rpg;

import org.bukkit.ChatColor;
import org.bukkit.event.EventHandler;
import org.bukkit.event.EventPriority;
import org.bukkit.event.Listener;
import org.bukkit.event.player.AsyncPlayerChatEvent;
import org.bukkit.event.player.PlayerRespawnEvent;
import org.bukkit.event.weather.WeatherChangeEvent;

public class OtherListener implements Listener {

	RPGPlugin plugin = RPGPlugin.getPlugin();

	@EventHandler(ignoreCancelled = true)
	public void onWeatherChange(final WeatherChangeEvent event) {
		event.setCancelled(true);
	}

	@EventHandler(priority = EventPriority.HIGHEST, ignoreCancelled = true)
	public void onChat(AsyncPlayerChatEvent event) {
		RPGPlayer Rpg = RPGPlayer.getRpgPlayerFromBukkitPlayer(event.getPlayer());
		String format = event.getFormat();
		format = format.replace("-lvl-", ChatColor.translateAlternateColorCodes('&', "&a" + Rpg.GetLevel()));
		String guild = Rpg.GetGuild();
		// Replace guild placeholder with
		if (guild != null) {
			format = format.replace("-Guild-",
					ChatColor.translateAlternateColorCodes('&', "&8(&e" + Rpg.GetGuild() + "&8) "));
		} else {
			format = format.replace("-Guild-", "");
		}
		// Replace class placeholder with class name
		if (Rpg.GetClass() != 0) {
			format = format.replace("-class-", ChatColor.translateAlternateColorCodes('&',
					" &" + Guild.GetColorFromClass(Rpg.GetClass()) + Rpg.getClassName()));
		} else {
			format = format.replace("-class-", "");
		}
		event.setFormat(format);
	}

    @EventHandler
	public void OnRespawn(PlayerRespawnEvent event) {

	}

}
