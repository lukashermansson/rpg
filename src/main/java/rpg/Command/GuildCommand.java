package rpg.Command;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;


import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

import rpg.Guild;
import rpg.GuildMember;
import rpg.LogLevel;
import rpg.Logger;
import rpg.RPGPlayer;
import rpg.RPGPlugin;


public class GuildCommand implements CommandExecutor {
	private RPGPlugin pl = RPGPlugin.getPlugin();

	@SuppressWarnings("unused")
	@Override
	public boolean onCommand(CommandSender sender, org.bukkit.command.Command cmd, String alias, String[] args) {
		if (cmd.getName().equalsIgnoreCase("guild")) {
			String UsageString = "Usage: /guild | Create | Member | Delete ";
			String UsageCreateString = "Usage: /guild Create <Name>";
			// we make shure there are enough args to keep going
			if (args.length > 0) {
				if (args[0].equalsIgnoreCase("create")) {
					if (sender instanceof Player) {
						if (sender.hasPermission("guild.create")) {
							// Check that the player is not in a guild
							if (args.length > 1) {

								try {
									Statement statement = pl.GetConnection().createStatement();
									// Player can create a guild
									PreparedStatement ps = pl.GetConnection()
											.prepareStatement("SELECT * FROM Guild WHERE GuildName=?;");
									ps.setString(1, args[1]);
									ResultSet res = ps.executeQuery();
									if (!res.next()) {
										// we dont have a guild with that name.
										ps = pl.GetConnection()
												.prepareStatement("INSERT INTO Guild (GuildName) VALUES (?);");
										ps.setString(1, args[1]);
										ps.executeUpdate();

										ps = pl.GetConnection()
												.prepareStatement("SELECT * FROM Guild WHERE GuildName=?;");
										ps.setString(1, args[1]);
										res = ps.executeQuery();
										res.next();
										Guild guild = new Guild(res.getInt("idGuild"));
										Player player = (Player) sender;
										guild.AddPlayerToGuild(player.getUniqueId() + "", 2);
										guild.UpdateDB();
										guild.AddLoggedInPlayer(player.getUniqueId() + "");
										Logger.Send("&aGuild " + args[1] + " created successfully.", sender);
										return true;
									} else {
										Logger.Send("A guild with that name already exists ", sender);
										return true;
									}
								} catch (SQLException e) {
									// TODO Auto-generated catch block
									e.printStackTrace();
									Logger.Send("An sql error has occured", sender);
									Logger.Logg("Mysql error in guild command for guild string: " + args[1],
											LogLevel.severe);
									return true;
								}

							} else {
								Logger.Send(UsageCreateString, sender);
								return true;
							}
						} else {
							// Player should get no perms message
							Logger.Send(pl.GetNopermMessage(), sender);
							return true;
						}
					} else {
						Logger.Send("You have to be a player to create a guild", sender);
						return true;
					}
				} else if (args[0].equalsIgnoreCase("Delete")) {
					// check if user is in a guild and has perms to delete it.
					Statement statement;
					try {
						statement = pl.GetConnection().createStatement();
						ResultSet res = statement.executeQuery("SELECT * FROM Guildmember WHERE player_UUID = '"
								+ ((Player) sender).getUniqueId() + "';");
						if (res.next()) {
							// player is in aa guild
							if (res.getInt("Role") >= 2) {
								// player has perms
								Guild guild = Guild.getGuildFromId(res.getInt("Guild"));
								if (guild != null) {
									guild.DeleteGuild();
								}
								Logger.Send("&cGuild deleted successfully.", sender);
								return true;
							} else {
								// player does not have perms
								Logger.Send("&cYou do not have permission to delete this guild.", sender);
								return true;
							}
						} else {
							// player is not in a guild
							Logger.Send("You are not a member of a guild.", sender);
							return true;
						}
					} catch (SQLException e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
						Logger.Send("An sql error has occured", sender);
						Logger.Logg("Mysql error in deleting guild", LogLevel.severe);
						return true;
					}
				} else if (args[0].equalsIgnoreCase("member")) {
					if (args.length > 1) {
						Statement statement;
						try {
							statement = pl.GetConnection().createStatement();
							ResultSet res = statement.executeQuery(
									"SELECT * FROM RPG.Guildmember join Guild where Guild.idGuild = Guildmember.Guild AND player_UUID = '"
											+ ((Player) sender).getUniqueId() + "';");
							if (res.next()) {
								if (args[1].equalsIgnoreCase("invite")) {
									// send an invite
									if (res.getInt("Role") >= 1) {
										// player can invite
										if (args.length > 2) {
											// a player is suplied
											Guild guild = Guild.getGuildFromId(res.getInt("Guild"));
											String playerUUID = RPGPlayer.getUUIDFromPlayerName(args[2]);
											if (playerUUID != null) {
												// we have a player with this
												// UUID
												// lets do sql here
												PreparedStatement ps1 = pl.GetConnection().prepareStatement(
														"SELECT * FROM GuildInvites WHERE UUID=? AND GuildID=? And DateSent >= NOW() - INTERVAL 30 minute;");
												ps1.setString(1, playerUUID);
												ps1.setInt(2, res.getInt("Guild"));
												ResultSet res1 = ps1.executeQuery();

												if (res1.next()) {
													// this player is invited
													// allready
													Logger.Send("&cThis player is allready invited to this guild",
															sender);
													return true;
												}
												if (guild != null && guild.GetGuildMemberFromUUID(playerUUID) != null) {
													// this player is in our
													// guild already
													Logger.Send("&cThis player is already a member of this guild",
															sender);
													return true;
												}
												if (playerUUID.equals(((Player) sender).getUniqueId().toString())) {
													Logger.Send("&cYou cant invite yourself to your guild", sender);
													return true;
												}
												// player is not invited so
												// lets invite the player
												PreparedStatement ps2 = pl.GetConnection().prepareStatement(
														"Insert into GuildInvites (UUID, GuildID) VALUES (?, ?);");
												ps2.setString(1, playerUUID);
												if (guild != null) {
													ps2.setInt(2, guild.getGuildId());
												}
												ps2.executeUpdate();

												// Lets do some code to notify
												// the player if online
												@SuppressWarnings("deprecation")
												Player onlinePlayer = pl.getServer().getPlayer(args[2]);
												if (onlinePlayer != null) {
													// player is also online
													if (guild != null) {
														Logger.Send("&aYou got an invite to join guild "
                                                                + guild.getGuildName() + " by: " + sender.getName() + ".",
                                                                onlinePlayer);
													}
												}
												Logger.Send("Successfully sent invite to player.", sender);
												ps2.close();
												res1.close();
												ps1.close();
												statement.close();
												return true;
											} else {
												Logger.Send("That is not a valid name", sender);
												return true;
											}
										} else {
											// to few args supplied
											Logger.Send("You must supply a player to invite", sender);
											return true;
										}

									} else {
										// player does not have perms to invite
										Logger.Send("&cYou do not have permission invite players to this guild.",
												sender);
										statement.close();
										return true;
									}
								} else if (args[1].equalsIgnoreCase("remove")) {
									if (res.getInt("Role") >= 1) {
										if (args.length > 2) {
											Guild guild = Guild.getGuildFromId(res.getInt("Guild"));
											PreparedStatement ps = pl.GetConnection()
													.prepareStatement("SELECT * FROM Names WHERE Name=?;");
											ps.setString(1, args[2]);
											ResultSet res1 = ps.executeQuery();
											if (res1.next()) {
												if (guild != null) {
													for (int i = 0; i < guild.GetMembers().size(); i++) {
                                                        if (guild.GetMembers().get(i).getUUID()
                                                                .equals(res1.getString("player_UUID"))) {
                                                            // Player is in guild,
                                                            // remove that player
                                                            if (guild.GetMembers().get(i).getRank() < res.getInt("Role")) {
                                                                guild.RemovePlayerFromGuild(guild.GetMembers().get(i));
                                                                Logger.Send("&aPlayer has ben removed from guild.", sender);
                                                                return true;
                                                            } else {
                                                                Logger.Send(
                                                                        "&cYou do not have permission to remove that player.",
                                                                        sender);
                                                                return true;
                                                            }
                                                        }
                                                    }
												}
												Logger.Send("&cNo player with that name is in this guild.", sender);
												return true;
											} else {
												// no player with that name
												Logger.Send("&cNo player with that name is in this guild.", sender);
												return true;
											}
										} else {
											Logger.Send("&cTo few args make sure to include player name.", sender);
											return true;
										}
									} else {
										// player does not have perms to invite
										Logger.Send("&cYou do not have permission remove players from this guild.",
												sender);
										return true;
									}
								} else if (args[1].equalsIgnoreCase("list")) {
									Guild guild = Guild.getGuildFromId(res.getInt("Guild"));
									PreparedStatement ps;
									if (guild != null) {
										Logger.Send("&cPlayers in guild " + guild.getGuildName() + ":", sender);
									}
									// loops all the guild members and prints
									// them. one sql request for all separate
									// players might get intensive.
									if (guild != null) {
										for (int i = 0; i < guild.GetMembers().size(); i++) {
                                            ps = pl.GetConnection()
                                                    .prepareStatement("SELECT * FROM Names WHERE player_UUID=?;");
                                            ps.setString(1, guild.GetMembers().get(i).getUUID());
                                            ResultSet res1 = ps.executeQuery();
                                            res1.next();
                                            // list player with ranking syntax
                                            Logger.Send(GuildMember.GetRankString(guild.GetMembers().get(i).getRank())
                                                    + " &c" + res1.getString("Name"), sender);
                                            ps.close();
                                            res1.close();

                                        }
                                        return true;
									}
								} else {
									// wrong args
									Logger.Send("Wrong usage: use /guild member Remove : list : invite", sender);
									return true;
								}
							} else {
								// player is not in a guild
								Logger.Send("&cYou are not in a guild", sender);
								statement.close();
								res.close();
								return true;
							}
						} catch (SQLException e) {
							Logger.Send("An sql error has occured", sender);
							Logger.Logg("Mysql error in command", LogLevel.severe);
							e.printStackTrace();
							return true;
						}
					} else {
						// to few args
						Logger.Send("to few arguments", sender);
						return true;
					}
				} else if (args[0].equalsIgnoreCase("invites")) {
					int page;
					int guildsPerPage = 5;
					int NumTotalPages = 0;
					if (args.length > 1) {
						// here we can handle accepting etc
						// this should break at all points
						// all ifs
						try {
							if (args[1].equalsIgnoreCase("accept")) {
								PreparedStatement ps1;
								ps1 = pl.GetConnection()
										.prepareStatement("SELECT * FROM Guildmember WHERE player_UUID=?");
								ps1.setString(1, ((Player) sender).getUniqueId() + "");
								ResultSet res1 = ps1.executeQuery();

								// Check if player is not in a guild
								if (!res1.next()) {
									if (args.length > 2) {

										// TODO: Fix SQL to also display is
										// thats a guild, maybe by doing a right
										// join.
										ps1 = pl.GetConnection().prepareStatement(
												"SELECT * FROM GuildInvites left join Guild on GuildID = Guild.idGuild WHERE UUID=? AND Guild.GuildName=? And DateSent >= NOW() - INTERVAL 30 minute;");
										ps1.setString(1, ((Player) sender).getUniqueId() + "");
										ps1.setString(2, args[2]);
										ResultSet res2 = ps1.executeQuery();

										if (res2.next()) {
											// this invite exists, lets remove
											// that invite and add us to that
											// guild.
											// Remove pending invites
											ps1 = pl.GetConnection().prepareStatement(
													"DELETE FROM GuildInvites WHERE UUID=? AND GuildID=?;");
											ps1.setString(1, ((Player) sender).getUniqueId() + "");
											ps1.setInt(2, res2.getInt("GuildID"));
											ps1.execute();

											// Add player to guild
											Guild g;
											if ((g = Guild.getGuildFromId(res2.getInt("GuildID"))) != null) {
												// guild is loaded lets load
												// player using guild method
												// broadcast first then
												g.SendGuildMessage(sender.getName()
														+ " has just joined the guild " + g.getGuildName());
												g.AddPlayerToGuild(((Player) sender).getUniqueId() + "", 0);
											} else {
												// guild is not loaded so lets
												// just do this using sql code

												// Lets load that guild, we are
												// going to add us to it anyway
												Guild guild = new Guild(res2.getInt("GuildID"));
												guild.AddPlayerToGuild(String.valueOf(((Player) sender).getUniqueId()), 0);

											}
											Logger.Send("&cSuccessfully joined guild " + args[2], sender);
										} else {
											Logger.Send("&cYou do not have an invite to join that guild", sender);
										}
										ps1.close();
										return true;

									} else {
										// to few args
										Logger.Send("To few args please specify guild to join", sender);
										return true;
									}

								} else {
									// Player is in a guild.
									Logger.Send("&cYou must leave your guild to join a new guild", sender);
								}
								// close this section
								ps1.close();
								res1.close();
								return true;
							} else {
								// The user specified any other arg than accept
								// so lets continue
								Logger.Send("&cYou did not specify accept" + args[2], sender);
							}
						} catch (SQLException e) {
							// TODO Auto-generated catch block
							e.printStackTrace();
							Logger.Send("&4MySQL error in command!", sender);
							return true;
						}

						// page handeling for invites

						int rpage;
						try {
							PreparedStatement ps1 = pl.GetConnection().prepareStatement(
									"SELECT count(*) FROM GuildInvites left join Guild on GuildID = Guild.idGuild WHERE UUID=? And DateSent >= NOW() - INTERVAL 30 minute;");
							ps1.setString(1, ((Player) sender).getUniqueId() + "");
							ResultSet res1 = ps1.executeQuery();
							res1.next();
							NumTotalPages = (int) Math.ceil((res1.getInt("count(*)")) / (guildsPerPage));
							rpage = Integer.parseInt(args[1]) - 1;
						} catch (NumberFormatException | SQLException e) {
							Logger.Send("&cInvalid number", sender);
							return true;
						}
						// check if number a valid number in our pages.
						if (rpage <= NumTotalPages && rpage >= 0) {
							page = rpage;
						} else {
							Logger.Send(
									"&cThat is not a vaild page number, vaild pages are: 1-" + (NumTotalPages + 1) + "",
									sender);
							return true;
						}

					} else {
						// we just want to list the invites
						page = 0;
					}
					// we want to show guilds as we know we only get here if we
					// are going by the list
					try {
						PreparedStatement ps1 = pl.GetConnection().prepareStatement(
								"SELECT *, TIMESTAMPDIFF(minute, DateSent, NOW()) as difftime, count(*) FROM GuildInvites left join Guild on GuildID = Guild.idGuild WHERE UUID=? And DateSent >= NOW() - INTERVAL 30 minute LIMIT ?, ?;");
						ps1.setString(1, ((Player) sender).getUniqueId() + "");
						ps1.setInt(2, page * guildsPerPage);
						ps1.setInt(3, page * guildsPerPage + guildsPerPage);
						ResultSet res1 = ps1.executeQuery();
						
						res1.next();
						if (res1.getInt("Count(*)") != 0) {
							Logger.Send("&aPending guild invites:", sender);
							res1.beforeFirst();
							while (res1.next()) {
								Logger.Send("&e" + res1.getString("GuildName") + " &a" + res1.getInt("difftime")
										+ "minutes ago", sender);
							}
							if ((NumTotalPages + 1) <= 2) {
								Logger.Send("&aShowing page " + page + " of " + NumTotalPages + "!", sender);
							}
						}else{
							Logger.Send("&cNo pending invites", sender);
						}
						return true;
					} catch (SQLException e) {
						Logger.Send("&4MySQL error in command!", sender);
						e.printStackTrace();
						return true;
					}
					
				} else {
					Logger.Send(UsageString, sender);
					return true;
				}
			} else {
				Logger.Send(UsageString, sender);
				return true;
			}
		}
		return false;
	}
}
