package rpg.Command;

import rpg.Logger;
import rpg.RPGPlugin;
import rpg.Setting;
import rpg.Settings;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

import java.util.ArrayList;

/**
 * Created by Lukas-PC on 2017-04-10.
 */
public class RPGCommand implements CommandExecutor {
    RPGPlugin plugin = RPGPlugin.getPlugin();
    Settings settingsManager = Settings.getSettingsInstance();
    @Override
    public boolean onCommand(CommandSender sender, Command command, String label, String[] args) {
        RPGPlugin pl = RPGPlugin.getPlugin();
        if (command.getName().equalsIgnoreCase("rpg")) {
            if(!sender.hasPermission("RPG.rpg")){
                Logger.Send(plugin.GetNopermMessage(), sender);
                return true;
            }
            if (args.length > 0) {
                if (args[0].equalsIgnoreCase("reload")) {
                    //handels reloads

                    settingsManager.pushSettingsToDB();
                    settingsManager.FetchSettings();
                    Logger.Send("&aReloaded settings", sender);
                    return true;
                }else if (args[0].equalsIgnoreCase("setspawn")) {
                    if (args.length > 3){
                        try {
                            int x = Integer.parseInt(args[1]);
                            int y = Integer.parseInt(args[2]);
                            int z = Integer.parseInt(args[3]);

                            settingsManager.updateSetting("spawn", x + ", " + y + ", " + z);
                            Logger.Send("&aSpawn sucsessfully set to there", sender);
                            return true;
                        } catch (NumberFormatException name) {
                            Logger.Send("&4You must provide cordinates", sender);
                            return true;
                        }

                    }else if(sender instanceof Player){
                        Player playersender = (Player) sender;
                        int x = playersender.getLocation().getBlockX();
                        int y = playersender.getLocation().getBlockY();
                        int z = playersender.getLocation().getBlockZ();

                        settingsManager.updateSetting("spawn", x + ", " + y + ", " + z);
                        Logger.Send("&aSpawn sucsessfully set to here", sender);
                        return true;
                    }

                }else if (args[0].equalsIgnoreCase("listSettings")) {
                    ArrayList<Setting> settings = settingsManager.GetSettings();
                    Logger.Send("Settings in Ram", sender);
                    for(int i = 0; settings.size() > i; i++){
                        Logger.Send("&aLabel: " + settings.get(i).getLabel() + " value:" + settings.get(i).getValue() , sender);
                    }
                    return true;
                }
            }
        }


        return false;
    }

}
