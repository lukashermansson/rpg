package rpg.Command;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

import org.bukkit.Bukkit;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

import rpg.Guild;
import rpg.Logger;
import rpg.RPGPlayer;
import rpg.RPGPlugin;

public class DebugCommand implements CommandExecutor {
	private RPGPlugin pl = RPGPlugin.getPlugin();

	@Override
	public boolean onCommand(CommandSender sender, org.bukkit.command.Command cmd, String alias, String[] args) {
		//debug command
		if (cmd.getName().equalsIgnoreCase("debug")) { 
			//sender is setting
			if(sender.hasPermission("debug.debug")){
				//we make shure there are enough args to keep going
				if(args.length > 0){
					if(args[0].equalsIgnoreCase("set")){
						//we want to set some value
						if(args.length > 1){
							if(args[1].equalsIgnoreCase("level")){
								//we want to set a level
								if(args.length > 2){
                                    @SuppressWarnings("deprecation")
									Player player = pl.getServer().getPlayer(args[2]);
									if(player == null){
										//we want to set to ourselves
										if(sender instanceof Player){
											//we know our executor is a player
											int level;
											try {
												level = Integer.parseInt(args[2]);
											}catch (NumberFormatException e){
												Logger.Send("&4That is not a valid integer", sender);
												return true;
											}
											RPGPlayer rpgPlayerInstance = RPGPlayer.getRpgPlayerFromBukkitPlayer((Player) sender);
											if (rpgPlayerInstance != null) {
												rpgPlayerInstance.SetLevel(level);
											}
											Logger.Send("&asucsessfully set level of " + sender.getName(), sender);
											return true;
										}else{
											Logger.Send("&4You cannot execute that as a console", sender);
											return true;
										}
									}else{
										//we want to set to a player
										int level;
										try {
											level = Integer.parseInt(args[3]);
										}catch (NumberFormatException e){
											Logger.Send("&4That is not a valid integer", sender);
											return true;
										}
										RPGPlayer rpgPlayerInstance = RPGPlayer.getRpgPlayerFromBukkitPlayer(player);
										if (rpgPlayerInstance != null) {
											rpgPlayerInstance.SetLevel(level);
										}
										Logger.Send("&asucsessfully set level of " + player.getName(), sender);
										return true;
									}
								}else{
									Logger.Send("&4to few arguments you must suply a value or a player", sender);
									return true;
								}
							}else if(args[1].equalsIgnoreCase("exp")){
								//we want to set some exp
								if(args.length > 2){
                                    @SuppressWarnings("deprecation")
									Player player = pl.getServer().getPlayer(args[2]);
									if(player == null){
										//we want to set to ourselves
										if(sender instanceof Player){
											//we know our executor is a player
											int exp;
											try {
												exp = Integer.parseInt(args[2]);
											}catch (NumberFormatException e){
												Logger.Send("&4That is not a valid integer", sender);
												return true;
											}
											RPGPlayer rpgPlayerInstance = RPGPlayer.getRpgPlayerFromBukkitPlayer((Player) sender);
											if (rpgPlayerInstance != null) {
												rpgPlayerInstance.SetExp(exp);
											}
											Logger.Send("&a successfully set exp of " + sender.getName(), sender);
											return true;
										}else{
											Logger.Send("&4You cannot execute that as a console", sender);
											return true;
										}
									}else{
										//we want to set to a player
										int exp;
										try {
											exp = Integer.parseInt(args[3]);
										}catch (NumberFormatException e){
											Logger.Send("&4That is not a valid integer", sender);
											return true;
										}
										RPGPlayer rpgPlayerInstance = RPGPlayer.getRpgPlayerFromBukkitPlayer(player);
                                        if (rpgPlayerInstance != null) {
                                            rpgPlayerInstance.SetExp(exp);
                                        }
                                        Logger.Send("&a successfully set exp of " + player.getName(), sender);
										return true;
									}
								}else{
									Logger.Send("&4to few arguments you must supply a value or a player", sender);
									return true;
								}
							}
						}else{
							Logger.Send("&4Too few arguments supply what type you want to set", sender);
							return true;
						}
					}else if(args[0].equalsIgnoreCase("add")){
						//we want to set some value
						if(args.length > 1){
							if(args[1].equalsIgnoreCase("level")){
								//we want to set a level
								if(args.length > 2){
                                    @SuppressWarnings("deprecation")
									Player player = pl.getServer().getPlayer(args[2]);
									if(player == null){
										//we want to add to ourselves
										if(sender instanceof Player){
											//we know our executor is a player
											int level;
											try {
												level = Integer.parseInt(args[2]);
											}catch (NumberFormatException e){
												Logger.Send("&4That is not a valid integer", sender);
												return true;
											}
											RPGPlayer rpgPlayerInstance = RPGPlayer.getRpgPlayerFromBukkitPlayer((Player) sender);
                                            if (rpgPlayerInstance != null) {
                                                rpgPlayerInstance.addLevel(level);
                                            }
                                            Logger.Send("&a successfully added level of " + sender.getName(), sender);
											return true;
										}else{
											Logger.Send("&4You cannot execute that as a console", sender);
											return true;
										}
									}else{
										//we want to set to a player
										int level;
										try {
											level = Integer.parseInt(args[3]);
										}catch (NumberFormatException e){
											Logger.Send("&4That is not a valid integer", sender);
											return true;
										}
										RPGPlayer rpg = RPGPlayer.getRpgPlayerFromBukkitPlayer(player);
                                        if (rpg != null) {
                                            rpg.addLevel(level);
                                        }
                                        Logger.Send("&asucsessfully added level of " + player.getName(), sender);
										return true;
									}
								}else{
									Logger.Send("&4to few arguments you must suply a value or a player", sender);
									return true;
								}
							}else if(args[1].equalsIgnoreCase("exp")){
								//we want to set some exp
								if(args.length > 2){
                                    @SuppressWarnings("deprecation")
									Player player = pl.getServer().getPlayer(args[2]);
									if(player == null){
										//we want to set to ourselves
										if(sender instanceof Player){
											//we know our executor is a player
											int exp;
											try {
												exp = Integer.parseInt(args[2]);
											}catch (NumberFormatException e){
												Logger.Send("&4That is not a valid integer", sender);
												return true;
											}
											RPGPlayer rpg = RPGPlayer.getRpgPlayerFromBukkitPlayer((Player) sender);
                                            if (rpg != null) {
                                                rpg.AddExp(exp);
                                            }
                                            Logger.Send("&a successfully added exp of " + sender.getName(), sender);
											return true;
										}else{
											Logger.Send("&4You cannot execute that as a console", sender);
											return true;
										}
									}else{
										//we want to set to a player
										int exp;
										try {
											exp = Integer.parseInt(args[3]);
										}catch (NumberFormatException e){
											Logger.Send("&4That is not a valid integer", sender);
											return true;
										}
										RPGPlayer rpg = RPGPlayer.getRpgPlayerFromBukkitPlayer(player);
                                        if (rpg != null) {
                                            rpg.AddExp(exp);
                                        }
                                        Logger.Send("&a successfully Added exp of " + player.getName(), sender);
										return true;
									}
								}else{
									Logger.Send("&4To few arguments you must supply a value or a player", sender);
									return true;
								}
							}else{
								Logger.Send("&4You must supply what valueType you want to set", sender);
								return true;
							}
						}
					}else if(args[0].equalsIgnoreCase("get")){
						//we want to set some value
						if(args.length > 1){
							if(args[1].equalsIgnoreCase("level")){
								//we want to get a level
								if(args.length < 3){
									//we want to get from ourselves
									if(sender instanceof Player){
										//we know our executor is a player
											
										RPGPlayer rpg = RPGPlayer.getRpgPlayerFromBukkitPlayer((Player) sender);
                                        if (rpg != null) {
                                            Logger.Send("&aLevel of: " + sender.getName() + " is " + rpg.GetLevel(), sender);
                                        }
                                        return true;
									}else{
										Logger.Send("&4You cannot execute that as a console", sender);
										return true;
									}
								}else{
                                    @SuppressWarnings("deprecation")
										Player player = pl.getServer().getPlayer(args[2]);
										if(player != null){
										//we want to get from a player
										RPGPlayer rpg = RPGPlayer.getRpgPlayerFromBukkitPlayer(player);
                                            if (rpg != null) {
                                                Logger.Send("&aLevel of: " + player.getName() + " is " + rpg.GetLevel(), sender);
                                            }
                                            return true;
									}else{
										Logger.Send("&4that is not a valid player", sender);
									}
								}
							}else if(args[1].equalsIgnoreCase("exp")){
								//we want to get some exp
								if(args.length < 3){
									//we want to get from ourselves
									if(sender instanceof Player){
										//we know our executor is a player
										RPGPlayer rpg = RPGPlayer.getRpgPlayerFromBukkitPlayer((Player) sender);

                                        if (rpg != null) {
                                            Logger.Send("&aExp of: " + sender.getName() + " is " + rpg.GetExp() + " out of " + rpg.xpToLevel(), sender);
                                        }
                                        return true;
									}else{
										Logger.Send("&4You cannot execute that as a console", sender);
										return true;
									}
								}else{
									//we want to set to a player
                                    @SuppressWarnings("deprecation")
									Player player = pl.getServer().getPlayer(args[2]);
									if(player != null){
										RPGPlayer rpg = RPGPlayer.getRpgPlayerFromBukkitPlayer(player);
                                        if (rpg != null) {
                                            Logger.Send("&aExp of: " + player.getName() + " is " + rpg.GetExp() + " out of " + rpg.xpToLevel(), sender);
                                        }
                                        return true;
									}else{
										Logger.Send("&4 that is not a valid player", sender);
										return true;
									}
								}
							
							}else if(args[1].equalsIgnoreCase("guilds")){
								int guildsPerPage = 10;
								int page = 0;
								int NumTotalguilds;
								int NumTotalpages = 0;
								//select how many guilds we have, this is to base our pages calculations of
								try {
									PreparedStatement ps;
									ps = pl.GetConnection().prepareStatement("SELECT count(*) FROM Guild;");
									ResultSet res1 = ps.executeQuery();
									
									res1.next();
									NumTotalguilds = res1.getInt("count(*)");
									NumTotalpages = (int) Math.ceil(NumTotalguilds / guildsPerPage);
								} catch (SQLException e1) {
									Logger.Logg("SQL error when fetching guilds from db");
									e1.printStackTrace();
								}
								
								if(args.length > 2){
									//user is specifying a page
									int rpage;
									//check if user inputed a valid number.
									try {
										rpage = Integer.parseInt(args[2]) -1;
									} catch (NumberFormatException e) {
										Logger.Send("&cInvalid number", sender);
										return true;
									}
									//check if number a valid number in our pages.
									if(rpage <= NumTotalpages && rpage >= 0){
										page = rpage;
									}else{
										Logger.Send("&cThat is not a vaild page number, vaild pages are: 1-" + (NumTotalpages+1) + "", sender);
										return true;
									}
								}
								PreparedStatement ps;
								try {
									//sql for selecting guilds and how many players that guild has.
									ps = pl.GetConnection().prepareStatement("SELECT idGuild, GuildName, Count(Guild) as guildmembers FROM Guild right join Guildmember on Guild.idGuild = Guildmember.Guild group by Guild LIMIT ?, ?;");
									ps.setInt(1, page*guildsPerPage);
									ps.setInt(2, page*guildsPerPage+guildsPerPage);
									ResultSet res1 = ps.executeQuery();
									//listing guilds
									Logger.Send("&aGuild list:", sender);
									while(res1.next()){
										//TODO: add code to change color depending if guild is loaded. possibly blue?
										String col = "&a";
										if (Guild.getGuildFromId(res1.getInt("idGuild")) != null){
											col = "&b";
										}
										Logger.Send(col + res1.getString("GuildName") + " ("
												+ res1.getString("guildmembers") + ").", sender);
									}
									// message to know how far you are into the pages
									Logger.Send("&aShowing page " +  (page +1) + "/" + (NumTotalpages+1) +  ".", sender);
									
									//closing the statements
									ps.close();
									res1.close();
									return true;
								} catch (SQLException e) {
									Logger.Logg("SQL error in fetching guilds and amount of members ");
									e.printStackTrace();
								}
							}
						}else{
							Logger.Send("&4Too few arguments supply what type you want to set", sender);
							return true;
						}
					}else{
						Logger.Send("&4That is not a valid option: valid options are | SET | ADD | GET |", sender);
						return true;
					}
				}else{
					Logger.Send("Usage: /debug | SET | ADD | GET |  level, exp <player> [value]", sender);
					return true;
				}
			}else{
				//player does not have permission to use that command
				Logger.Send("&4You do not have permission to do that", sender);
				return true;
			}
			return true;
		}
		return false; 
	}
}
