package rpg.Command;

import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

import rpg.Logger;
import rpg.RPGPlayer;

@SuppressWarnings("unused")
public class BanCommand implements CommandExecutor {

	@SuppressWarnings("FieldCanBeLocal")
	private boolean temp = false;

	// Ban command will take victim and optionaly reason and time.
	@SuppressWarnings("unused")
	@Override
	public boolean onCommand(CommandSender sender, org.bukkit.command.Command cmd, String alias, String[] args) {
		if (cmd.getName().equalsIgnoreCase("ban")) {
			String usageString = "&aUsage: /ban | PERM | TEMP |<player> [reason] xD xM xS";
			if (args.length > 0) {
				String reason = null;
				String UUID;
				String Executor = null;
				boolean temp = false;
				String VictimName;
				if (sender instanceof Player) {
					//noinspection UnusedAssignment
					Executor = ((Player) sender).getUniqueId().toString();
				}
				// we set temp to true here this will decide what arg place to
				// look for in the future
				if (args[0].equalsIgnoreCase("Temp"))
					temp = true;
				if (args.length > getArgOffset()) {

					VictimName = args[getArgOffset()];

					UUID = RPGPlayer.getUUIDFromPlayerName(VictimName);

					// Checking so player is in database. otherwise an error
					// message will apear and command will be aborted.
					if (UUID == null) {
						Logger.Send("&4The name was not able to be resolved to a uuid properly", sender);
						return true;
					}

					//noinspection StatementWithEmptyBody
					if (args.length <= 1 + getArgOffset()) {
						// set reason to null
						if (temp) {
							// wrong usage wee need if no reason info atleast
							// the | sign.
							Logger.Send("&cYou must if nothing else suply the time " + usageString, sender);
							return true;
						}
					} else {
						// player has suplied a suposed reason
						// build reason until any date information. separate
						// there with a | symbol for simplicity

					}
				} else {
					// We did not get a player so abort and send usage string.
					Logger.Send("&cNo player suplied " + usageString, sender);
				}

			} else {
				// Need atleast 2 args to do this command
				Logger.Send("&cTo few arguments " + usageString, sender);
				return true;
			}
		}

		return false;
	}

	// Returns offset for command starting from first arg
	private int getArgOffset() {
		int offset = 0;
		if (temp)
			offset++;

		return offset;
	}
}
