package rpg;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;

import mysql.mysql.MySQL;
import org.bukkit.Bukkit;
import org.bukkit.Location;
import org.bukkit.entity.Player;
import org.bukkit.inventory.ItemStack;
import org.bukkit.plugin.java.JavaPlugin;
import rpg.Command.DebugCommand;
import rpg.Command.GuildCommand;
import rpg.Command.RPGCommand;


public class RPGPlugin extends JavaPlugin {
	private static RPGPlugin plugin = null;
	public static String pluginString = "&8[&3RPG&8]&f";
	public static String permString = "RPG";
	static Location spawn = null;
	String Mainworld = "World";
	private Settings SettingsManager;
	// dont worry this is a local login, i can use this very weak password
	// TODO: Secure just in case
	private MySQL MySQL = new MySQL("192.168.1.6", "3306", "RPG", "RPG",
			"rpg");
	private Connection c = null;
	
	@Override
		public void onEnable() {
		plugin = this;
		Logger.Logg("&aLoading Plugin, loading online players into memmory", LogLevel.log);
		DBConnect();
		SettingsManager = new Settings();
		spawn = getSpawn();
		for (Player player : Bukkit.getServer().getOnlinePlayers()) {
			RPGPlayer rpg = new RPGPlayer(player);
			rpg.registerPlayer();
		}

		//Load guilds from db or memmory
		for (Player player : Bukkit.getServer().getOnlinePlayers()) {
			try {
				Statement statement;
				statement = GetConnection().createStatement();
				ResultSet res = statement.executeQuery("SELECT Guild FROM Guildmember WHERE player_UUID = '" + player.getUniqueId() + "';");
				if(res.next()){
					int GuildId = res.getInt("Guild");
					if(Guild.getGuildFromId(GuildId) != null){
						//Guild is loaded
						Logger.Logg("Loading guild from memmory", LogLevel.debug);
						Guild guild = Guild.getGuildFromId(GuildId);
						guild.AddLoggedInPlayer(player.getUniqueId() + "");
					}else{
					//guild is not loaded, create one
						Logger.Logg("Creating guild to memory", LogLevel.debug);
						Guild guild = new Guild(GuildId);
						guild.AddLoggedInPlayer(player.getUniqueId() + "");
					}
				}
			} catch (SQLException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
		getServer().getPluginManager().registerEvents(new LoginListener(), plugin);
		getServer().getPluginManager().registerEvents(new DamageListner(), plugin);
		getServer().getPluginManager().registerEvents(new OtherListener(), plugin);

		Logger.Logg("Scedualing auto save", LogLevel.log);
		Bukkit.getScheduler().scheduleSyncRepeatingTask(plugin, new Runnable() {
			@Override
			public void run() {
				//Logger.Logg("Syncing with DataBase", LogLevel.debug);
				for (Player player : Bukkit.getServer().getOnlinePlayers()) {
					RPGPlayer rpg = RPGPlayer.getRpgPlayerFromBukkitPlayer(player);
					rpg.UpdateDB();
				}
				Guild.UpdateAllToDB();
			}
			// wait a minute before starting the timer, there is nothing to save
			// here yet
		}, 20 * 60L, 20 * 60L);

		Bukkit.getScheduler().scheduleSyncRepeatingTask(plugin, new Runnable() {
			@Override
			public void run() {
				for (Player player : Bukkit.getServer().getOnlinePlayers()) {
					RPGPlayer rpg = RPGPlayer.getRpgPlayerFromBukkitPlayer(player);
					rpg.regen();
				}
			}
			// wait a minute before starting the timer, there is nothing to save
			// here yet
		}, 20 * 1L, 20 * 1L);
		getCommand("debug").setExecutor(new DebugCommand());
		getCommand("ban").setExecutor(new DebugCommand());
		getCommand("guild").setExecutor(new GuildCommand());
		getCommand("rpg").setExecutor(new RPGCommand());

	}
	public static RPGPlugin getPlugin(){
		return plugin;
	}
	public String getWorld(){
		return Mainworld;
	}
	
	public String GetNopermMessage(){
		return SettingsManager.getSetting("NoPermMessage").Value;
	}

	private Location getSpawn(){
		Setting spawnSetting = SettingsManager.getSetting("spawn");
		//
		String Cords = spawnSetting.getValue().replace(" ", "");
		String[] CordsArrayStr = Cords.split(",");
		int[] CordsArrayInt = new int[3];
		for(int i = 0;i < CordsArrayStr.length;i++){
			CordsArrayInt[i] = Integer.parseInt(CordsArrayStr[i]);
		}
		return new Location(Bukkit.getWorld(Mainworld), CordsArrayInt[0], CordsArrayInt[1], CordsArrayInt[2]);
	}

	private void DBConnect() {
		try {
			c = MySQL.openConnection();
			Logger.Logg("&cSetting up connection to database", LogLevel.log);
		} catch (Exception e) {
			e.printStackTrace();
			Logger.Logg("Unable connect to db aboarting", LogLevel.severe);
			plugin.getServer().shutdown();
		}

	}

	@Override
	public void onDisable() {
		Logger.Logg("&4Shuting down saving player data", LogLevel.log);
		// unregisters all players from memory resulting in a push to db
		Guild.UpdateAllToDB();
		for (Player player : Bukkit.getServer().getOnlinePlayers()) {
			RPGPlayer rpg = RPGPlayer.getRpgPlayerFromBukkitPlayer(player);
			rpg.UnregisterPlayer();
		}
		// closing server connection to bd
		try {
			c.close();
		} catch (SQLException e) {
			Logger.Logg("Unable to disconnect from db", LogLevel.severe);
			e.printStackTrace();
		}
	}

	// Dont need to pass the connection the plugin instance is enough
	public Connection GetConnection() {
		try {
			if(!c.isValid(2)){
				DBConnect();
			}
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		return c;
	}
	
	//TODO: Add code to check on db what item does what damge 
	public double GetItemDamage(ItemStack item){
		return 0;
		
	}
	
	//this will update all stored name and make a request for all players stored in our database.
	//If the server starts growing this might get intensive both for us and for mojangs servers.
	//A better aproatch them might be to get rid or the updating code and add a time feild to our names table
	//and never ever update names older then x months maby. they might not be intresting to keep track of after sutch inactivity.
	
	
	public void UpdateAllPlayerNamesInDB(){
		
	}
}
