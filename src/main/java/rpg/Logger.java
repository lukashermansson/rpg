package rpg;

import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

public class Logger {

	static boolean debug = true;
	public static void Logg(String message) {
		Logg(message,LogLevel.debug);
	}
	public static void Logg(String message, LogLevel level) {
		switch (level) {
		case all:
			Bukkit.getServer().broadcastMessage(
					ChatColor.translateAlternateColorCodes('&', RPGPlugin.pluginString + "&8: &f" + message));
			break;
		case severe:
			for (Player player : Bukkit.getServer().getOnlinePlayers()) {
				if (player.hasPermission(RPGPlugin.permString + ".log.severe")) {
					player.sendMessage(
							ChatColor.translateAlternateColorCodes('&', RPGPlugin.pluginString + "&8: &4" + message));
				}
			}
			Bukkit.getConsoleSender().sendMessage(
					ChatColor.translateAlternateColorCodes('&', RPGPlugin.pluginString + "&8: &4" + message));
			break;
		case warning:
			for (Player player : Bukkit.getServer().getOnlinePlayers()) {
				if (player.hasPermission(RPGPlugin.permString + ".log.warning")) {
					player.sendMessage(
							ChatColor.translateAlternateColorCodes('&', RPGPlugin.pluginString + "&8: &c" + message));
				}
			}
			Bukkit.getConsoleSender().sendMessage(
					ChatColor.translateAlternateColorCodes('&', RPGPlugin.pluginString + "&8: &c" + message));
			break;
		case log:
			Bukkit.getConsoleSender().sendMessage(
					ChatColor.translateAlternateColorCodes('&', RPGPlugin.pluginString + "&8: &f" + message));
			break;
		case debug:
			if (debug) {
				Bukkit.getConsoleSender().sendMessage(
						ChatColor.translateAlternateColorCodes('&', RPGPlugin.pluginString + "&8: &e" + message));
			}
			break;
		default:
			break;
		}
	}

	public static void Send(String message, CommandSender sender) {
		sender.sendMessage(ChatColor.translateAlternateColorCodes('&', RPGPlugin.pluginString + "&8: &f" + message));
	}
	public static void Send(String message, Player player) {
		Send(message, (CommandSender) player);
	}
	public static void Send(String message, RPGPlayer player) {
		Send(message, (CommandSender) player.getPlayer());
	}
}
