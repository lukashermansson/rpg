package rpg;

public class GuildMember {
	private String UUID;
	private int Rank;

	GuildMember(String UUID, int Rank) {
		this.setUUID(UUID);
		this.setRank(Rank);
	}

	public int getRank() {
		return Rank;
	}

	private void setRank(int rank) {
		Rank = rank;
	}

	public String getUUID() {
		return UUID;
	}

	private void setUUID(String uUID) {
		UUID = uUID;
	}

	/*
	 * Rank string from id Rank list: 0: Normal/member 1: Commander/mod never
	 * use mod tho might get confusing 2: Leader/Admin only the creator/owner
	 * have this
	 * 
	 * returns a color code and a sign separate methods to stip sign from color
	 * 
	 */
	public static String GetRankString(int rank) {
		return GetRankColor(rank) + GetRankCode(rank);
	}

	/*
	 * Get color from rank this need to be matched with above code. or this will
	 * be inconsistent.
	 * 
	 */
	/*
	TODO: add rank color and code to database
	 */
	private static String GetRankColor(int rankId) {
		switch (rankId) {
		case 0:
			return "&8";
		case 1:
			return "&f";
		case 2:
			return "&b";
		default:
			return null;
		}
	}

	/*
	 * Get indicator sign. This needs to be matched with above code or this will
	 * be inconsitent.
	 * 
	 */
	private static String GetRankCode(int rank) {
		switch (rank) {
		case 0:
			return "";
		case 1:
			return "*";
		case 2:
			return "**";
		default:
			return null;
		}
	}
}
