package rpg;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;

    public class Settings {
        private static Settings instance = null;
        private ArrayList<Setting> settings = new ArrayList<>();
        Settings(){
            instance = this;
            settings = LoadSettingsFromDB();

            //Default settings these are overwritten after first boot
            addSetting(new Setting("spawn", "100, 100, 100"));
            addSetting(new Setting("maintenance", "false"));
            addSetting(new Setting("NoPermMessage", "NoPerm"));


            pushSettingsToDB();
        }
        public void FetchSettings(){
            settings = LoadSettingsFromDB();
        }

        public static Settings getSettingsInstance(){
            return instance;
        }

        public ArrayList<Setting> GetSettings(){
            return settings;
        }

        public void pushSettingsToDB(){
            for (Setting setting : settings) {
                try {
                    PreparedStatement ps = RPGPlugin.getPlugin().GetConnection()
                            .prepareStatement("INSERT INTO Settings (label, value) VALUES (?, ?) ON DUPLICATE KEY UPDATE value=?;");
                    ps.setString(1, setting.getLabel());
                    ps.setString(2, setting.getValue());
                    ps.setString(3, setting.getValue());
                    ps.executeUpdate();

                    ps.close();
                } catch (SQLException e) {

                    e.printStackTrace();
                }
            }
        }
        public void updateSetting(String label, String value){
            Setting s = getSetting(label);
            s.Value = value;
        }

        Setting getSetting(String label){
            for (Setting setting : settings) {
                if (setting.label.equals(label)) {
                    return setting;
                }
            }
            return null;
        }

        private void addSetting(Setting setting){
            if(getSetting(setting.getLabel()) == null){
                settings.add(setting);
            }
        }
        private ArrayList<Setting> LoadSettingsFromDB(){
            ArrayList<Setting> DBSettings =  new ArrayList<>();
            try {
                PreparedStatement ps = RPGPlugin.getPlugin().GetConnection()
                        .prepareStatement("SELECT * FROM Settings;");
                ResultSet res1 = ps.executeQuery();
                while (res1.next()) {
                    DBSettings.add(new Setting(res1.getString("label"), res1.getString("value")));
                }
                ps.close();
                res1.close();
            } catch (SQLException e) {
                e.printStackTrace();
            }
            return DBSettings;

        }
    }

