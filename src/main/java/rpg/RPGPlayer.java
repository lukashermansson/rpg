package rpg;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.Iterator;
import java.util.List;

import org.bukkit.Bukkit;
import org.bukkit.entity.Player;
import org.bukkit.inventory.Inventory;

public class RPGPlayer {
	static List<RPGPlayer> registry = new ArrayList<RPGPlayer>();
	private Player player;
	private RPGPlugin plugin;
	private int Level;
	private long Experience;
	private int Class;
	private double health;
	private int defence;

	// Constructor for loading a player into memory

	public RPGPlayer(Player p) {
		player = p;

		plugin = RPGPlugin.getPlugin();
		health = 20;
		// loading a player and if it doesn't exist in the db create a player

		player.setFoodLevel(20);
		player.getInventory().clear();
		player.teleport(RPGPlugin.spawn);
		try {
			Statement statement = plugin.GetConnection().createStatement();
            //TODO: update to prepared statement 
			ResultSet res = statement.executeQuery("SELECT * FROM player WHERE UUID = '" + player.getUniqueId() + "';");
			if (!res.next()) {
				// New player adding uuid and current time
				DateFormat dateFormat = new SimpleDateFormat("yyyyMMddHHmmss");
				Date date = new Date();
				statement.executeUpdate("INSERT INTO player (`UUID`, `Joindate`) VALUES ('" + p.getUniqueId() + "', "
						+ dateFormat.format(date) + ");");
				Logger.Logg("Initial db request for new player: " + player.getName(), LogLevel.debug);
			}
			statement.close();
			res.close();
            //TODO: update to prepared statement 
			statement = plugin.GetConnection().createStatement();
			statement.executeUpdate("INSERT INTO Names (player_UUID, Name) VALUES('" + player.getUniqueId() + "', '"
					+ player.getName() + "') ON DUPLICATE KEY UPDATE Name='" + player.getName() + "';");
			statement.close();
			statement = plugin.GetConnection().createStatement();
			res = statement.executeQuery("SELECT * FROM player WHERE UUID = '" + player.getUniqueId() + "';");
			res.next();
			// Setting player data to stored values
			Level = res.getInt("Level");
			Experience = res.getInt("Experience");
			Class = res.getInt("ClassID");
			String inventoryDataRaw = res.getString("InventoryData");
			statement.close();
			res.close();

			List<String> DataList = new ArrayList<String>();
			// check so inventory data is not null before we try to split it
			if (inventoryDataRaw != null) {
				String[] InventoryData = inventoryDataRaw.split("¤");
				for (int i = 1; i < InventoryData.length; i++) {
					DataList.add(InventoryData[i]);
				}
			}
			// load the inventory data into the player
			Inventory inv = null;
			Inventory armorData = null;
			Inventory data = null;

			data = BukkitSerialization.toInventory((DataList), player);

			// sets content to expected inventory content.
			if (data != null) {
				// setting contents to a null will NPE
				p.getInventory().setContents(data.getContents());
			}

			UpdateXpBar();
		} catch (SQLException e) {
			Logger.Logg("Unable to make initial statement to db for player: " + player.getName(), LogLevel.warning);
			e.printStackTrace();
		}
	}

	String GetGuild() {
		Statement statement;
		try {
			statement = plugin.GetConnection().createStatement();
            //TODO: update to use prepared statement 
			ResultSet res = statement.executeQuery("SELECT Guild FROM Guildmember WHERE player_UUID = '"
					+ this.getBukkitPlayer().getUniqueId() + "';");
			if (res.next()) {
				int GuildId = res.getInt("Guild");
				Guild guild = Guild.getGuildFromId(GuildId);
				if (guild != null) {
					return guild.getGuildName();
				}
			}

		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		return null;
	}

	// get class name from classId
	String getClassName() {
		String Returns = null;
		try {
		Statement statement = plugin.GetConnection().createStatement();
            //TODO: update to use prepared stetement.
			ResultSet res = statement.executeQuery("SELECT Name FROM Class where ClassID =" + Class + ";");
			res.next();
			Returns = res.getString("Name");
			statement.close();
			res.close();
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return Returns;
	}

	private int CalcMaxHealth() {
		// TODO: create a mathematical model to get health dependant on class
		// armor stats and level
		return 20;
	}

	private float CalcArmor() {
		// TODO: create a mathematical model to get defence depending on armor
		// and level?
		return 1F;
	}

	public int getDefence() {
		return defence;
	}

	// returning 0 means no leveling can be done
	public long xpToLevel() {
		// functions from wow
		int level = this.GetLevel();
		if (level < 11) {
			return Math.round(40 * Math.pow(level, 2) + 360 * level);
		} else if (level < 28) {
			return Math.round(-.4 * Math.pow(level, 3) + 40.4 * Math.pow(level, 2) + 396 * level);
		} else if (level < 60) {
			return Math.round((65 * Math.pow(level, 2) - 165 * level - 6750) * 0.82);
		} else {
			return 0;
		}
	}

	// will damage the player a certain amount
	void damagePlayer(double d, org.bukkit.entity.Entity entity) {
		d = (double) Math.ceil(d * CalcArmor());
		health = health - d;
		Logger.Logg("Player " + player.getName() + " took " + d + " damage!", LogLevel.debug);
		UpdateLife();
	}

	void UpdateXpBar() {
		float fraction = ((float) GetExp() / (float) xpToLevel());
		player.setExp(fraction);
		player.setLevel(GetLevel());
	}


	// will make sure that the player will see his current life in his healthbar
	private void UpdateLife() {
		if ((health < 1)) {
			health = CalcMaxHealth();
			Logger.Send("You died", player);
			Logger.Logg("&ePlayer " + player.getName() + " died", LogLevel.all);
			// TODO: death haneling
			player.teleport(Bukkit.getWorld("World").getSpawnLocation());
		}
		player.setHealth(Math.ceil((health / CalcMaxHealth()) * 20));
	}

	void regen() {
		// TODO: calc regen amount
		double val = 5;
		if ((health + val) < CalcMaxHealth()) {
			health = health + val;
		}
		UpdateLife();
	}

	// returning the instanced player
	Player getPlayer() {
		return player;
	}

	// register player to list of active players
	void registerPlayer() {
		registry.add(this);
	}

	// unregisters the player from active players and update player to the
	// database
	void UnregisterPlayer() {
		UpdateDB();
		registry.remove(this);
	}

	void UpdateDB() {
		// Here all player data should get pushed to server
		// This should be called on shutdown for all players and individually
		// for disconnects
		// also scheduled in case of crash
		Statement statement;
		try {
			statement = plugin.GetConnection().createStatement();
			String IpAddress = player.getAddress().toString();
			IpAddress = IpAddress.replace("/", "");
			IpAddress = IpAddress.split(":")[0];
			List<String> invList = BukkitSerialization.toString(player.getInventory());
			StringBuilder SerializedInv = new StringBuilder();
			for (String ItemString : invList) {
				SerializedInv.append("¤").append(ItemString);
			}
                //TODO: update to use prepared stetement.
			statement.executeUpdate("UPDATE player SET Level=" + Level + ", Experience=" + Experience + ", ClassID='"
					+ Class + "', LastIP=INET_ATON('" + IpAddress + "'), InventoryData='" + SerializedInv + "' WHERE UUID='"
					+ player.getUniqueId() + "';");
			statement = plugin.GetConnection().createStatement();
                //TODO: update to use prepared stetement.
			statement.executeUpdate("INSERT INTO Names (player_UUID, Name) VALUES('" + player.getUniqueId() + "', '"
					+ player.getName() + "') ON DUPLICATE KEY UPDATE Name='" + player.getName() + "';");
			statement.close();
		} catch (SQLException e) {
			// We have a problem the server was not able to update player;
			// notice Admin
			Logger.Logg("Unable to update database for player: " + player.getName(), LogLevel.severe);
			e.printStackTrace();
		}
		// Logger.Logg("Updated database for player: " + player.getName(),
		// LogLevel.debug);
	}

	public long GetExp() {
		// returns player EXP
		return Experience;
	}

	Player getBukkitPlayer() {
		return player;

	}

	public void SetExp(long l) {
		// TODO: fix bug where leveling multiple times will resoult in wrong
		// order of message
		// Level Logic
		Experience = l;
		long neededExp = 0;
		long currentExp = 0;
		neededExp = xpToLevel();
		currentExp = GetExp();
		if (GetExp() > xpToLevel()) {
			// player has leveled

			long rest = currentExp - neededExp;
			addLevel(1);
			int clevel = GetLevel();
			Experience = 0;
			Logger.Logg("Player Level up: " + player.getName(), LogLevel.debug);
			Logger.Send("&aYou Leveled up, now level " + clevel, getPlayer());
			AddExp(rest);
		}
		UpdateXpBar();
	}

	public int GetLevel() {
		// returns player level
		return Level;
	}

	// will set level
	public void SetLevel(int value) {
		Level = value;
		player.setLevel(GetLevel());
		SetExp(0);
	}

	// will add level to internal level
	public void addLevel(int value) {
		SetLevel(GetLevel() + value);
	}

	public void AddExp(long Value) {
		// Adding to exp going through update handler
		SetExp(GetExp() + Value);
	}

	int GetClass() {
		return Class;
	}

	// Code to get the current player of the running class
	public static RPGPlayer getRpgPlayerFromBukkitPlayer(Player player) {
        // Loop though all registered players until current instance matches up
		// with current player
        for (RPGPlayer rpg : registry) {
            if (rpg.getPlayer() == player)
                return rpg;
        }
        return null;
    }

	// returns a name from an uuid and ensures that all conections are closed.
	public static String getPlayerNameFromUUID(String UUID) {
		String Returns = null;

		try {
			PreparedStatement ps = RPGPlugin.getPlugin().GetConnection()
					.prepareStatement("SELECT * FROM Names WHERE player_UUID=?;");
			ps.setString(1, UUID);
			ResultSet res1 = ps.executeQuery();

			Returns = res1.getString("Name");
			ps.close();
			res1.close();
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		return Returns;

	}
	
	public double GetBaseDamage(){
		//BaseDamageIs a function dependant on level and class
		
		double BaseBaseDamage = 0;
		
		BaseBaseDamage = Math.pow(Level, 2) / 2;
		
		
		
		int factor = 0;
		
		
		
		try {
			PreparedStatement ps = RPGPlugin.getPlugin().GetConnection()
					.prepareStatement("SELECT * FROM Class WHERE ClassID=?;");
			ps.setInt(1, Class);
			
			ResultSet res1 = ps.executeQuery();
			if (res1.next()) {
				factor = res1.getInt("DamageFactor");
			}
			ps.close();
			res1.close();
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

        return BaseBaseDamage / factor;
		
	}

	// takes in a player uuid of the database and gets the current known name.
	public static String getUUIDFromPlayerName(String PlayerName) {
		String Returns = null;

		try {
			PreparedStatement ps = RPGPlugin.getPlugin().GetConnection()
					.prepareStatement("SELECT * FROM Names WHERE Name=?;");
			ps.setString(1, PlayerName);
			ResultSet res1 = ps.executeQuery();
			if (res1.next()) {
				Returns = res1.getString("player_UUID");
			}
			ps.close();
			res1.close();
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		return Returns;
	}

}
