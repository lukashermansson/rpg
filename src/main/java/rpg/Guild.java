package rpg;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;

import java.util.List;
import java.util.Objects;

import org.bukkit.ChatColor;

/**
 * @author lukas hermansson
 *
 */
public class Guild {
	private static List<Guild> registry = new ArrayList<>();
	private int guildId;
	private String GuildName;
	private RPGPlugin pl = RPGPlugin.getPlugin();
	private ArrayList<RPGPlayer> OnlinePlayer = new ArrayList<>();
	private ArrayList<GuildMember> Members = new ArrayList<>();

	// constructor

	public Guild(int guildID) {
		Statement statement;
		try {
			registry.add(this);

			statement = pl.GetConnection().createStatement();

			// load guild from db
			ResultSet res = statement.executeQuery("SELECT * FROM Guild WHERE idGuild='" + guildID + "';");
			res.next();
			this.guildId = res.getInt("idGuild");
			setGuildName(res.getString("GuildName"));

			ResultSet members = statement.executeQuery("SELECT * FROM RPG.Guildmember where Guild='" + guildID + "';");
			while (members.next()) {
				Members.add(new GuildMember(members.getString("player_UUID"), members.getInt("Role")));
			}
			// debug line for logging all guild members at init
			/*
			 * for(int j = 0; j < Members.size(); j++){
			 * Logger.Logg(Members.get(j).getUUID(), LogLevel.debug); }
			 */
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	// delete guild object but make shure to delete guild f
	public void DeleteGuild() {
		// Do SQL
		try {
			Statement statement = pl.GetConnection().createStatement();
			statement.execute("DELETE FROM GuildInvites WHERE GuildID=" + guildId + ";");
			statement = pl.GetConnection().createStatement();
			statement.execute("DELETE FROM Guildmember WHERE Guild=" + guildId + ";");
			statement = pl.GetConnection().createStatement();
			statement.execute("DELETE FROM Guildmember WHERE Guild=" + guildId + ";");
			statement = pl.GetConnection().createStatement();
			statement.execute("DELETE FROM Guild WHERE idGuild=" + guildId + ";");
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			return;
		}
		Members = null;
		OnlinePlayer = null;
		registry.remove(this);

	}

	// get guild member from string Note check if player is online first
	/**
	 * @param UUID Player UUID
	 * @return GuildMember if exists in guild or null otherwise
	 */
	public GuildMember GetGuildMemberFromUUID(String UUID) {
		for (GuildMember Member : Members) {
			if (Objects.equals(Member.getUUID(), UUID)) {
				return Member;
			}
		}
		return null;
	}

	
	
	// Update information to database
	/**
	 * Pushes changes to database
	 */
	public void UpdateDB() {
		// Push changes to server
		// Loop all members and update their ranks etc.
		try {
			//Updates guildMembers
			for (GuildMember Member : Members) {
				// insert into guildMember
                PreparedStatement ps = RPGPlugin.getPlugin().GetConnection()
                        .prepareStatement("REPLACE INTO Guildmember (Guild, player_UUID, Role) VALUES (?, ?, ?);");
                ps.setInt(1, guildId);
                ps.setString(2, Member.getUUID());
                ps.setInt(3, Member.getRank());

                ps.executeUpdate();
                ps.close();
			}
			//Update Guild Information
			PreparedStatement ps = RPGPlugin.getPlugin().GetConnection()
					.prepareStatement("REPLACE INTO Guild (idGuild, GuildName) VALUES (?, ?);");
			ps.setInt(1, guildId);
			ps.setString(2, getGuildName());

			ps.executeUpdate();
			ps.close();
		} catch (SQLException e) {
			// Unable to update for guild
			Logger.Logg("SQL Exeption during GuildUpdate to DB for guild with ID: " + guildId + " and name: " + getGuildName());
			e.printStackTrace();
		}

	}

	// add player to guild and try to load him
	public void AddPlayerToGuild(String UUID, int role) {
		GuildMember Member = new GuildMember(UUID, role);
		Members.add(Member);
		if (AttemptLoadOfPlayer(Member)) {
			// success
			Logger.Logg("Successfully loaded online player after adding to db", LogLevel.debug);
		}
		UpdateDB();
	}

	// Try to add a player from a string con trary to member object in
	// AttemptLoadOfPlayer
	public void AddLoggedInPlayer(String UUID) {

		GuildMember Member = null;
		try {
			Statement statement = pl.GetConnection().createStatement();
			ResultSet members = statement
					.executeQuery("SELECT Role FROM Guildmember where player_UUID ='" + UUID + "';");
			if (members.next()) {
				Member = new GuildMember(UUID, members.getInt("Role"));
			}
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		if (Member != null) {
			AttemptLoadOfPlayer(Member);
		}
	}

	// will try to unload guild but only if its empty
    void TryUnload() {
		// Logger.Logg(OnlinePlayer.size() + "", LogLevel.debug);
		if (OnlinePlayer.size() < 1) {
			Logger.Logg("Onloaded guild " + this.getGuildName(), LogLevel.debug);
			UpdateDB();
			registry.remove(this);
		}
	}
	
	//Method to broadcast a message to a guild
	public void SendGuildMessage(String message){
		for(int i = 0; i < Members.size(); i++){
			OnlinePlayer.get(i).getBukkitPlayer().sendMessage(ChatColor.translateAlternateColorCodes('&', RPGPlugin.pluginString + "&8: &f" + message));
		}
	}
	// Takes a guild member and loads into online players
	private boolean AttemptLoadOfPlayer(GuildMember GuildMember) {
		for (int j = 0; j < RPGPlayer.registry.size(); j++) {
			if (Objects.equals(RPGPlayer.registry.get(j).getBukkitPlayer().getUniqueId() + "", GuildMember.getUUID())) {
				OnlinePlayer.add(RPGPlayer.registry.get(j));
				return true;
			}
		}
		return false;

	}

	// should not be a problem.
	public void RemovePlayerFromGuild(GuildMember GuildMember) {
		Members.remove(GuildMember);
		AttemptRemoveOfJustOnlinePlayer(GuildMember);
		// remove from db too.
		try {
			Statement statement = pl.GetConnection().createStatement();
			statement.execute("DELETE FROM Guildmember WHERE player_UUID = " + GuildMember.getUUID());
		} catch (SQLException e) {
			// faled to remove guildmemeber from db
			e.printStackTrace();
		}
	}

	// online player remove note this does not remove player from members table
    void AttemptRemoveOfJustOnlinePlayer(GuildMember GuildMember) {
		for (int j = 0; j < RPGPlayer.registry.size(); j++) {
			if (Objects.equals(RPGPlayer.registry.get(j).getBukkitPlayer().getUniqueId() + "", GuildMember.getUUID())) {
				OnlinePlayer.remove(j);
                return;
			}
		}

    }

	// get guild id.
	public int getGuildId() {
		return guildId;
	}

	// get the name of the guild
	public String getGuildName() {
		return GuildName;
	}

	// GuildName is pushed to server
    private void setGuildName(String guildName) {
		GuildName = guildName;
	}

	// just return the member array
	public ArrayList<GuildMember> GetMembers() {
		return Members;
	}

	// static to loop though all loaded guilds and push to database
	static void UpdateAllToDB() {
        // Loop though all registered players until current instance matches up
		// with current player
        for (rpg.Guild Guild : registry) {
            Guild.UpdateDB();
        }
	}

	/*
	Todo: fix magic number add to batabase.
	 */
	static String GetColorFromClass(int class1) {
		switch (class1) {
		case 1:
			return "a";
		case 2:
			return "c";
		default:
			return "8";
		}
	}

	// static to return this object from id Note this does not work for unloaded
	// guilds
	public static Guild getGuildFromId(int id) {
        // Loop though all registered players until current instance matches up
		// with current player
        for (rpg.Guild Guild : registry) {
            if (Guild.getGuildId() == id)
                return Guild;
        }
		return null;
	}

}
