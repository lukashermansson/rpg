package rpg;

import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.entity.EntityDamageByEntityEvent;
import org.bukkit.event.entity.EntityDamageEvent;
import org.bukkit.event.entity.EntityDamageEvent.DamageCause;
import org.bukkit.event.entity.EntityRegainHealthEvent;
import org.bukkit.event.entity.FoodLevelChangeEvent;
import org.bukkit.event.player.PlayerRespawnEvent;

public class DamageListner implements Listener {
	@SuppressWarnings("unused")
	private RPGPlugin plugin = RPGPlugin.getPlugin();
	// constructor for this class

	// Make sure normal damage dosent apply instead use internal method of
	// damaging player
	@EventHandler
	public void onPlayerdamaged(EntityDamageByEntityEvent event) {

		if (event.getEntity() instanceof Player) {
			// this is a player
			Player player = (Player) event.getEntity();
			RPGPlayer rpg = RPGPlayer.getRpgPlayerFromBukkitPlayer(player);
			rpg.damagePlayer(event.getDamage(), event.getDamager());
			event.setDamage(0);
		} else {
			// its some other entity
		}
	}

	// disable normal envouremental damage
	@EventHandler
	public void envourment(EntityDamageEvent event) {
		if (event.getEntity() instanceof Player) {
			if (event.getCause() == DamageCause.DROWNING || event.getCause() == DamageCause.STARVATION
					|| event.getCause() == DamageCause.FALL || event.getCause() == DamageCause.LAVA
					|| event.getCause() == DamageCause.FIRE || event.getCause() == DamageCause.FIRE_TICK) {
				event.setCancelled(true);
			}
		}
	}

	// make shure food never drains
	@EventHandler
	public void Food(FoodLevelChangeEvent event) {
		event.setFoodLevel(20);
		event.setCancelled(true);
	}

	// if a player where to normaly die somehow this triggers
	@EventHandler
	public void Respawn(PlayerRespawnEvent event) {
		RPGPlayer rpg = RPGPlayer.getRpgPlayerFromBukkitPlayer(event.getPlayer());
		rpg.UpdateXpBar();
	}

	// disable normal health regen
	@EventHandler
	public void regen(EntityRegainHealthEvent event) {
		if (event.getEntity() instanceof Player) {
			event.setCancelled(true);
		}
	}
}
